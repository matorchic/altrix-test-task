const validate = require("validate-fields")();

const schemas = {
  "auth" : {
    "login" : "string(1,50)",
    "password" : "string(1,50)"
  },
  "getEventList": {
    "token" : "string(1,50)"
  },
  "getEventById" : {
    "token" : "string(1,50)"
  },
  "createEvent" : {
    "token" : "string(1,50)",
    "name" : "string(1,50)",
    //TODO add timestamp or whatsoever, UTC atm
    "start_date" : "numericUint",
    "end_date" : "numericUint"
  },
  "updateEvent" : {
    "token" : "string(1,50)",
    "id" : "string(1,50)",
    "name?" : "string(1,50)",
    //TODO add timestamp or whatsoever, UTC atm
    "start_date?" : "numericUint",
    "end_date?" : "numericUint"
  }
};

class Validator {
  constructor() {
    this.models = {

    };
    for (let key in schemas) {
      this.models[key] = validate.parse(schemas[key]);
    }
  }
  _validate(key,req) {
    let data = (req.method === 'GET' ? req.urlParsed : req.body);
    return this.models[key].validate(data, {strict: true});
  }
  _check(key, req, res, next) {
    if (this._validate(key, req))
      return next();
    res.status(400).send("Bad request");
  }
  auth(req,res,next) {this._check("auth",req,res,next)}
  getEventList(req,res,next) {this._check("getEventList",req,res,next)}
  getEventById(req,res,next) {this._check("getEventById",req,res,next)}
  createEvent(req,res,next) {
    if (!this._validate("createEvent",req))
      return res.status(400).send("Bad request");

    let startDate = +req.body.start_date,
      endDate = +req.body.end_date;

    if (startDate > endDate)
      return res.status(400).send("Bad request");

    return next();

  }
  updateEvent(req,res,next) {
    if (!this._validate("updateEvent",req))
      return res.status(400).send("Bad request");
    if (req.body.start_date && req.body.end_date) {
      let startDate = +req.body.start_date,
        endDate = +req.body.end_date;

      if (startDate > endDate)
        return res.status(400).send("Bad request");
    }
    return next();
  }
}

module.exports = Validator;