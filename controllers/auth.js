const UserModel = require("../models/user");
const bcrypt = require("bcrypt");

const possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
  tokenLength = 10;
  // , saltRounds = 10;

class AuthController {
  constructor(db) {
    this.model = new UserModel(db);
  }
  //Test purposes only, not an actual register
  /*
  register(login, password) {
    return bcrypt.hash(password, saltRounds)
      .then((hashedPwd) => {
        return this.model.create({login: login, password: hashedPwd});
      })
  }
  */
  authorize(req,res,next) {
    let login = req.body.login,
      password = req.body.password;
    this.checkLoginPassword(login, password)
      .then((token) => {
        res.status(200).json({token:token})
      })
      .catch((er) => {
        if (typeof er === 'string')
          return res.status(403).send(er);
        next(er);
      });
  }
  isAuthorized(req,res,next) {
    let token = (req.method === 'GET' ? req.query.token : req.body.token);
    this.checkAuthorized(token)
      .then(() => next())
      .catch((er) => {
        if (typeof er === 'string')
          return res.status(403).send(er);
        next(er);
      });
  }
  checkLoginPassword(login, password) {
    let userId, newToken;
    return this
      .model
      .getByLogin(login)
      .then((user) => {
        if (!user)
          throw "Wrong username or password";
        userId = user.id;
        return bcrypt.compare(password, user.password);
      })
      .then((equals) => {
        if (!equals)
          throw "Wrong username or password";
        newToken = AuthController._makeID(tokenLength);
        //Token + id should be stored in Redis, don't have time for that atm though
        return this.model.updateToken(userId, newToken)
      })
      .then(() => "" + newToken + userId);
  }
  checkAuthorized(token) {
    let parsedToken = AuthController._parseToken(token);
    if (!parsedToken)
      return Promise.reject("Not authorized!");

    return this
      .model
      .getById(parsedToken.uid)
      .then((user) => {
        if (!user || parsedToken.token != user.token) {
          console.error(`Wrong token! got ${parsedToken.token} while expecting ${user ? user.token : '[didn\'t find user'}`);
          throw "Not authorized!";
        }
      })
  }
  static _makeID(num) {
    let id = "";
    for(let i=0; i < +num; i++)
      id += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length));
    return id;
  }
  static _parseToken(tokenUID) {
    if (!tokenUID || tokenUID.toString().length <= tokenLength)
      return null;
    return {
      token: tokenUID.toString().slice(0,tokenLength),
      uid: tokenUID.toString().slice(tokenLength)
    }
  };
}

module.exports = AuthController;