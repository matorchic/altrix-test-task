const EventModel = require("../models/event");

class EventController {
  constructor(db) {
    this.model = new EventModel(db);
  }
  list(req,res,next) {
    return this
      .model
      .list(req.urlParsed.offset, req.urlParsed.limit)
      .then((eventList) => {
        res.status(200).json({events: eventList});
      })
      .catch(next);
  }
  getById(req,res,next) {
    return this
      .model
      .getById(req.params.id)
      .then((event) => {
        res.status(200).json({event: event});
      })
      .catch(next);
  }
  update(req,res,next) {
    if (!req.body.name && !req.body.start_date && !req.body.end_date)
      return res.status(204).send("OK");
    return this
      .model
      .update(req.body.id, req.body)
      .then(() => res.status(200).send("OK"))
      .catch(next);
  }
  create(req,res,next) {
    return this
      .model
      .create(req.body)
      .then(() => res.status(200).send("OK"))
      .catch(next);
  }
}

module.exports = EventController;