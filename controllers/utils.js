const url = require('url');

class ControllerUtils {
  static parseURL(req,res,next) { req.urlParsed = url.parse(req.url,true).query; next() };
  static lastErrorHandler(err,req,res,next) { res.status(500).send("Internal error!"); }
}

module.exports = ControllerUtils;