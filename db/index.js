const pg = require('pg-promise');
const bluebird = require('bluebird');

let dbConnection = null;

module.exports = (env) => {
  if (dbConnection === null) {
    let db = pg({
      promiseLib : bluebird
    }),
    config = require('../config')(env).db;
    dbConnection = db(config);
  }
  return dbConnection;
};