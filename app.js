const express = require("express");
const http = require('http');
const bodyParser = require('body-parser');

const	app = express();
const Router = require("./routes");
const config = require('./config')(app.get('env'));

let dbConnection = require("./db")(app.get('env'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

Router.setRoutes(app, dbConnection);

http
	.createServer(app)
	.listen(config.port, (er) => {
		if (er) {
      console.error("App did not init correctly!", er);
      process.exit(-1);
		}
    console.log(`App started on port:  ${config.port}; current mode: ${app.get('env')}`);
  });
