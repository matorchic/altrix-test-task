
const AuthController = require('../controllers/auth');
const ValidationController = require('../controllers/validate_request');
const EventController = require('../controllers/event');
const ControllerUtils = require('../controllers/utils');

class Router {
  static setRoutes (app, db) {
    const authController = new AuthController(db);
    const eventController = new EventController(db);
    const validationController = new ValidationController();

    app.post(
      '/auth',
      validationController.auth.bind(validationController),
      authController.authorize.bind(authController)
    );

    app.get(
      '/events',
      ControllerUtils.parseURL,
      validationController.getEventList.bind(validationController),
      authController.isAuthorized.bind(authController),
      eventController.list.bind(eventController)
    );

    app.get(
      '/events/:id',
      ControllerUtils.parseURL,
      validationController.getEventById.bind(validationController),
      authController.isAuthorized.bind(authController),
      eventController.getById.bind(eventController)
    );

    app.post(
      '/events',
      validationController.createEvent.bind(validationController),
      authController.isAuthorized.bind(authController),
      eventController.create.bind(eventController)
    );

    app.put(
      '/events/:id',
      (rq,rs,nx) => {rq.body.id = rq.params.id; nx();},
      validationController.updateEvent.bind(validationController),
      authController.isAuthorized.bind(authController),
      eventController.update.bind(eventController)
    );

    app.use('/', ControllerUtils.lastErrorHandler);

  }
}
module.exports = Router;