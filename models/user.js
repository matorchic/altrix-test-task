const BaseModel = require("./base");

class UserModel extends BaseModel {
  constructor(db) {
    super(db);
    this.tableName = "users";
    this.modifiedFields = ["login", "password", "token"];
  }
  getByLogin(login) {
    return this.db.oneOrNone("SELECT id, login, password FROM users WHERE login=$1",[login])
  }
  create(_data) {
    let data = {};
    if (_data.hasOwnProperty("login")) data.login = _data.login;
    if (_data.hasOwnProperty("password")) data.password = _data.password;
    return super.create(data);
  }
  updateToken(id, token) {
    return this.update(id, {token : token});
  }
}

module.exports = UserModel;