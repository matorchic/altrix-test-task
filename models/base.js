class BaseModel {
  constructor(db) {
    this.db = db;
    this.tableName = "";
    this.modifiedFields = [];
  }
  list(offset, limit) {
    let limitQuery = `LIMIT $1 ${offset ? 'OFFSET $2' : ''}`,
      data = offset ? [limit, offset] : [limit];
    return this.db.any(`SELECT * FROM ${this.tableName} ${limitQuery}`,data);
  }
  getById(id) {
    return this.db.oneOrNone(`SELECT * FROM ${this.tableName} WHERE id=$1`,[id]);
  }
  create(data) {
    let dataSet = [],
      fieldSet = "(",
      parenthesisSet = "(",
      index = 1;

    this.modifiedFields.forEach((field) => {
      if (data.hasOwnProperty(field)) {
        dataSet.push(data[field]);
        fieldSet += field + ", ";
        parenthesisSet += "$" + (index++) + ", ";
      }
    });

    fieldSet = fieldSet.slice(0, fieldSet.length - 2) + ")";
    parenthesisSet = parenthesisSet.slice(0, parenthesisSet.length - 2) + ")";

    let query = `INSERT INTO ${this.tableName} ${fieldSet} VALUES ${parenthesisSet}`;

    return this.db.none(query, dataSet);
  }
  update(id, data) {
    let
      dataSet = [],
      index = 1,
      fieldSet = this.modifiedFields
      .reduce((acc,field) => {
          if (data.hasOwnProperty(field)) {
            dataSet.push(data[field]);
            return acc + `${field}=$${index++},`
          }
          return acc;
        }
      ,"");
    fieldSet = fieldSet.slice(0, fieldSet.length - 1);

    let query = `UPDATE ${this.tableName} SET ${fieldSet} WHERE id=$${index}`;

    return this.db.any(query, dataSet.concat([id]));
  }
}

module.exports = BaseModel;