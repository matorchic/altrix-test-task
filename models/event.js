const BaseModel = require("./base");

class EventModel extends BaseModel {
  constructor(db) {
    super(db);
    this.tableName = "events";
    this.modifiedFields = ["name", "start_date", "end_date"];
  }
}

module.exports = EventModel;