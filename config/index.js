const configs = require("./config.json");

module.exports = (env) => env ? (configs[env] || configs["development"]) : configs["development"];